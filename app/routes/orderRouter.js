const express = require("express");
const router = express.Router();

const orderController = require("../controllers/orderController");

router.post("/devcamp-pizza365/orders", orderController.createOrder);

module.exports = router;