//Import thư viện mongoose
const mongoose = require("mongoose");

//class schema từ thư viện mongoose
const Schema = mongoose.Schema;

//khỏi tạo voucherSchema từ class schema
const voucherSchema = new Schema ({
    maVoucher: {
        type:String, 
        unique: true, 
        required:true
    },

	phanTramGiamGia:{
        type: Number, 
        required: true
    },
	ghiChu: {
        type: String, 
        required: false
    }
});

//Biên dịch voucher model từ voucherSchema
module.exports = mongoose.model("Voucher", voucherSchema);